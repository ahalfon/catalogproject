package jb.harel.catalogproject.controller;

import jb.harel.catalogproject.beans.Item;
import jb.harel.catalogproject.beans.ItemType;
import jb.harel.catalogproject.exceptions.InvalidEntityException;
import jb.harel.catalogproject.exceptions.InvalidOperationException;
import jb.harel.catalogproject.services.SportsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("sport")
@RequiredArgsConstructor
public class SportsController {
    private final SportsServiceImpl sportsService;

    @PostMapping
    public ResponseEntity<?> addItem(@RequestBody Item item) throws InvalidOperationException, InvalidEntityException {
        sportsService.addItem(item);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<?> getAllSportItems() {
        return new ResponseEntity<>(sportsService.getAllItemsByType(ItemType.SPORTS), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getSingleItem(@PathVariable Long id) throws InvalidEntityException,InvalidOperationException {
        return new ResponseEntity<>(sportsService.getSingleItem(id), HttpStatus.OK);
    }

    @GetMapping("num")
    public Long num(){
        return sportsService.getNumberOfItemsByType(ItemType.SPORTS);
    }

    @PutMapping
    public ResponseEntity<?> updateItem(@RequestBody Item item) throws InvalidOperationException, InvalidEntityException {
        sportsService.updateItem(item);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
