package jb.harel.catalogproject.controller;

import jb.harel.catalogproject.beans.Item;
import jb.harel.catalogproject.exceptions.InvalidEntityException;
import jb.harel.catalogproject.exceptions.InvalidOperationException;
import jb.harel.catalogproject.services.AdminServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("admin")
@RequiredArgsConstructor
public class AdminController {
    private final AdminServiceImpl adminService;

    @PostMapping
    public ResponseEntity<?> addItem(@RequestBody Item item) throws InvalidOperationException, InvalidEntityException {
        adminService.addItem(item);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<?> getAllItems() {
        return new ResponseEntity<>(adminService.getAllItems(), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getSingleItem(@PathVariable Long id) throws InvalidEntityException,InvalidOperationException {
        return new ResponseEntity<>(adminService.getSingleItem(id), HttpStatus.OK);
    }

    @GetMapping("num")
    public Long num(){
        return adminService.getNumberOfItems();
    }

    @PutMapping
    public ResponseEntity<?> updateItem(@RequestBody Item item) throws InvalidOperationException, InvalidEntityException {
        adminService.updateItem(item);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteItem(@PathVariable Long id) throws InvalidEntityException {
        adminService.deleteItem(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
