package jb.harel.catalogproject.controller;

import jb.harel.catalogproject.beans.Item;
import jb.harel.catalogproject.beans.ItemType;
import jb.harel.catalogproject.exceptions.InvalidEntityException;
import jb.harel.catalogproject.exceptions.InvalidOperationException;
import jb.harel.catalogproject.services.ElectricityServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("electricity")
@RequiredArgsConstructor
public class ElectricityController {
    private final ElectricityServiceImpl electricityService;

    @PostMapping
    public ResponseEntity<?> addItem(@RequestBody Item item) throws InvalidOperationException, InvalidEntityException {
        electricityService.addItem(item);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<?> getAllElectricItems() {
        return new ResponseEntity<>(electricityService.getAllItemsByType(ItemType.ELECTRICITY), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getSingleItem(@PathVariable Long id) throws InvalidEntityException,InvalidOperationException {
        return new ResponseEntity<>(electricityService.getSingleItem(id), HttpStatus.OK);
    }

    @GetMapping("num")
    public Long num(){
        return electricityService.getNumberOfItemsByType(ItemType.ELECTRICITY);
    }

    @PutMapping
    public ResponseEntity<?> updateItem(@RequestBody Item item) throws InvalidOperationException, InvalidEntityException {
        electricityService.updateItem(item);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
