package jb.harel.catalogproject.model;

import jb.harel.catalogproject.beans.Item;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListOfItems {
    private List<Item> items =new ArrayList<>();
}
