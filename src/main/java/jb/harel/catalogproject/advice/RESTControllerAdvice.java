package jb.harel.catalogproject.advice;

import jb.harel.catalogproject.exceptions.InvalidEntityException;
import jb.harel.catalogproject.exceptions.InvalidOperationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ControllerAdvice
public class RESTControllerAdvice {

    @ExceptionHandler(value = {InvalidEntityException.class, InvalidOperationException.class}) //After throw an exception
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDetails handleException(Exception e){
        return new ErrorDetails("Error",e.getMessage());
    }

}
