package jb.harel.catalogproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatalogprojectApplication {
	public static void main(String[] args) {
		SpringApplication.run(CatalogprojectApplication.class, args);
	}
}
