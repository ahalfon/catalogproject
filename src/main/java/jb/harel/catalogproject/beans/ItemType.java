package jb.harel.catalogproject.beans;

public enum ItemType {
    SPORTS,
    ELECTRICITY,
    FOOD,
    OTHER
}
