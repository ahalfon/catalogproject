package jb.harel.catalogproject.exceptions;

public class InvalidOperationException extends Exception {
    public InvalidOperationException(String msg) {
        super(msg);
    }
}
