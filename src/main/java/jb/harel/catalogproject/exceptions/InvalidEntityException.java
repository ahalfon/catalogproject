package jb.harel.catalogproject.exceptions;

public class InvalidEntityException extends Exception {
    public InvalidEntityException(String msg) {
            super(msg);
    }
}
