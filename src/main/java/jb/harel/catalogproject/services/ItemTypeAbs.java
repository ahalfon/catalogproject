package jb.harel.catalogproject.services;

import jb.harel.catalogproject.beans.ItemType;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
//@RequiredArgsConstructor
public abstract class ItemTypeAbs  {
    private /*final*/ ItemType itemType;
}
