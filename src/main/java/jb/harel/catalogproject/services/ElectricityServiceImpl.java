package jb.harel.catalogproject.services;

import jb.harel.catalogproject.beans.ItemType;
import jb.harel.catalogproject.repos.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ElectricityServiceImpl extends AdminServiceImpl {
    @Autowired
    ItemRepository itemRepository;

    public ElectricityServiceImpl() {
        super.setItemType(ItemType.ELECTRICITY);
    }
}
