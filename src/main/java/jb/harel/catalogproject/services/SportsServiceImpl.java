package jb.harel.catalogproject.services;

import jb.harel.catalogproject.beans.ItemType;
import jb.harel.catalogproject.repos.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SportsServiceImpl extends AdminServiceImpl {

    @Autowired
    ItemRepository itemRepository;

    public SportsServiceImpl() {
        super.setItemType(ItemType.SPORTS);
    }
}
