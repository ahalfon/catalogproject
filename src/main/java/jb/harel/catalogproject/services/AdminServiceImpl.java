package jb.harel.catalogproject.services;

import jb.harel.catalogproject.beans.Item;
import jb.harel.catalogproject.beans.ItemType;
import jb.harel.catalogproject.exceptions.InvalidEntityException;
import jb.harel.catalogproject.exceptions.InvalidOperationException;
import jb.harel.catalogproject.repos.ItemRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
//@RequiredArgsConstructor
@Primary
public class AdminServiceImpl implements AdminService {
    //private final ItemRepository itemRepository;
    @Autowired
    ItemRepository itemRepository;

    private ItemType itemType;

    @Override
    public void addItem(Item item) throws InvalidEntityException,InvalidOperationException {
        if(itemType!=null && item.getItemType()!=this.getItemType()) {
            throw new InvalidOperationException("cannot add an item outside your domain");
        }

        Long id = item.getId();
        if (id!=null && itemRepository.existsById(id)) {
            throw new InvalidEntityException("Item Already exists");
        }
        itemRepository.save(item);
    }

    @Override
    public void updateItem(Item updateItem) throws InvalidEntityException,InvalidOperationException {
        Item dbItem=itemRepository.findItemById(updateItem.getId());
        if(itemType!=null && dbItem.getItemType()!=this.getItemType()) {
            throw new InvalidOperationException("cannot update an item outside your domain");
        }

        Long id = updateItem.getId();
        if (id!=null && !itemRepository.existsById(id)) {
            throw new InvalidEntityException("Cannot update not existing id");
        }
        itemRepository.saveAndFlush(updateItem);
    }

    @Override
    public void deleteItem(Long id) throws InvalidEntityException {
        if (id!=null && !itemRepository.existsById(id)) {
            throw new InvalidEntityException("cannot delete - id not exist");
        }
        itemRepository.deleteById(id);
    }

    @Override
    public List<Item> getAllItems() {
        return itemRepository.findAll();
    }


    public Item getSingleItem2(ItemType itemType, Long id) throws InvalidEntityException,InvalidOperationException {
        if(itemType!=null && itemType!=this.getItemType()) {
            throw new InvalidOperationException("cannot get an item outside your domain");
        }

        return itemRepository.findById(id).orElseThrow(() -> new InvalidEntityException("Item not found"));
    }

    @Override
    public Long getNumberOfItems() {
        return itemRepository.count();
    }

    @Override
    public Long getNumberOfItemsByType(ItemType itemType) {
        return itemRepository.countAllByItemType(itemType.toString());
    }

    @Override
    public List<Item> getAllItemsByType(ItemType itemType) {
        return itemRepository.findAllByItemType(itemType.toString());
    }

    @Override
    public Item getSingleItem(Long id) throws InvalidEntityException,InvalidOperationException {
        Item item=itemRepository.findById(id).orElseThrow(() -> new InvalidEntityException("Item not found"));

        if(itemType!=null && item.getItemType()!=this.getItemType()) {
            throw new InvalidOperationException("cannot get an item outside your domain");
        }
        return item;
    }
}
