package jb.harel.catalogproject.services;

import jb.harel.catalogproject.beans.Item;
import jb.harel.catalogproject.beans.ItemType;
import jb.harel.catalogproject.exceptions.InvalidEntityException;
import jb.harel.catalogproject.exceptions.InvalidOperationException;

import java.util.List;

public interface AdminService {
    void setItemType(ItemType itemType);
    void addItem(Item item) throws InvalidEntityException,InvalidOperationException;
    void updateItem(Item item) throws InvalidEntityException,InvalidOperationException;
    void deleteItem(Long id) throws InvalidEntityException;
    List<Item> getAllItems();
    Item getSingleItem(Long id) throws InvalidEntityException,InvalidOperationException;
    List<Item> getAllItemsByType(ItemType itemType);
    Long getNumberOfItems();
    Long getNumberOfItemsByType(ItemType itemType);
}
