package jb.harel.catalogproject.repos;

import jb.harel.catalogproject.beans.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Long> {
    @Query(value = "SELECT * FROM items where item_type=:itemType ",nativeQuery = true)
    List<Item> findAllByItemType(String itemType);

    @Query(value = "SELECT count(*) FROM items where item_type=:itemType ",nativeQuery = true)
    Long countAllByItemType(String itemType);

    Item findItemById(Long id);
}
