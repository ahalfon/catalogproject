package jb.harel.catalogproject.clr;

import jb.harel.catalogproject.beans.Item;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Component
@RequiredArgsConstructor
@Order(4)
public class ElectricityControllerTesting implements CommandLineRunner {
    private final RestTemplate restTemplate;
    private final static String URL = "http://localhost:8080/electricity";

    @Override
    public void run(String... args) throws Exception {
        //ListOfItems res = restTemplate.getForObject(URL, ListOfItems.class); //didnt work, not sure why
        Item[] res = restTemplate.getForObject(URL, Item[].class); //workaround , worked
        Arrays.stream(res).forEach(System.out::println);
    }
}
