package jb.harel.catalogproject.clr;

import jb.harel.catalogproject.beans.Item;
import jb.harel.catalogproject.model.ListOfItems;
import jb.harel.catalogproject.services.AdminServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
@Order(2)
public class AdminServiceTesting implements CommandLineRunner {
    private final AdminServiceImpl adminServiceImpl;

    @Override
    public void run(String... args) throws Exception {
        List<Item> items = adminServiceImpl.getAllItems();
        items.forEach(System.out::println);
    }
}
