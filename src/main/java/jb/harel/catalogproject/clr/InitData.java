package jb.harel.catalogproject.clr;

import jb.harel.catalogproject.beans.Item;
import jb.harel.catalogproject.beans.ItemType;
import jb.harel.catalogproject.repos.ItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;

@Component
@RequiredArgsConstructor
@Order(1)
public class InitData implements CommandLineRunner {
    private final ItemRepository itemRepository;

    @Override
    public void run(String... args) throws Exception {

        Item item1 = Item.builder()
            .id(Long.valueOf(1L))
            .createdDate(new Timestamp(System.currentTimeMillis()))
            .itemType(ItemType.FOOD)
            .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
            .name("Pasta")
            .price(BigDecimal.valueOf(10l))
            .build();

        Item item2 = Item.builder()
                .id(Long.valueOf(2L))
                .createdDate(new Timestamp(System.currentTimeMillis()))
                .itemType(ItemType.FOOD)
                .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
                .name("Salad")
                .price(BigDecimal.valueOf(118l))
                .build();

        Item item3 = Item.builder()
                .id(Long.valueOf(3L))
                .createdDate(new Timestamp(System.currentTimeMillis()))
                .itemType(ItemType.FOOD)
                .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
                .name("Pizza")
                .price(BigDecimal.valueOf(8l))
                .build();

        Item item4 = Item.builder()
                .id(Long.valueOf(4L))
                .createdDate(new Timestamp(System.currentTimeMillis()))
                .itemType(ItemType.ELECTRICITY)
                .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
                .name("Oven")
                .price(BigDecimal.valueOf(899l))
                .build();

        Item item5 = Item.builder()
                .id(Long.valueOf(5L))
                .createdDate(new Timestamp(System.currentTimeMillis()))
                .itemType(ItemType.ELECTRICITY)
                .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
                .name("Microwave")
                .price(BigDecimal.valueOf(20))
                .build();

        Item item6 = Item.builder()
                .id(Long.valueOf(6L))
                .createdDate(new Timestamp(System.currentTimeMillis()))
                .itemType(ItemType.ELECTRICITY)
                .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
                .name("TV")
                .price(BigDecimal.valueOf(298))
                .build();

        Item item7 = Item.builder()
                .id(Long.valueOf(7L))
                .createdDate(new Timestamp(System.currentTimeMillis()))
                .itemType(ItemType.SPORTS)
                .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
                .name("Football")
                .price(BigDecimal.valueOf(11))
                .build();

        Item item8 = Item.builder()
                .id(Long.valueOf(8L))
                .createdDate(new Timestamp(System.currentTimeMillis()))
                .itemType(ItemType.SPORTS)
                .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
                .name("Baseball")
                .price(BigDecimal.valueOf(19))
                .build();

        Item item9 = Item.builder()
                .id(Long.valueOf(9L))
                .createdDate(new Timestamp(System.currentTimeMillis()))
                .itemType(ItemType.OTHER)
                .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
                .name("Mouse")
                .price(BigDecimal.valueOf(199))
                .build();

        Item item10 = Item.builder()
                .id(Long.valueOf(10L))
                .createdDate(new Timestamp(System.currentTimeMillis()))
                .itemType(ItemType.OTHER)
                .lastModifiedDate(new Timestamp(System.currentTimeMillis()))
                .name("Cat")
                .price(BigDecimal.valueOf(49))
                .build();


        itemRepository.saveAll(Arrays.asList(item1,item2,item3,item4,item5,item6,item7,item8,item9,item10));

        itemRepository.findAll().forEach(System.out::println);
    }
}
